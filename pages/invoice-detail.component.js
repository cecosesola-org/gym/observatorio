import {
    html,
    css,
    LitElement
} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

const { interval } = rxjs;
const { 
	tap,
    take,
    catchError,
    switchMap,
    map,
} = rxjs.operators;

export class InvoiceDetailPage extends LitElement {
	static properties = {
		invoiceId: '',
		invoice: {
			id: 0,
			cqc: '',
			items: [],
			total: {
				ved: 0,
				usd: 0
			},
			payed: {
				ved: 0,
				usd: 0
			},
			difference: {
				ved: 0,
				usd: 0
			}
		},
	}

	constructor() {
		super();
	}

	render() {
		return html`
			<h1>Factura en espera</h1>
			<h2>Nos pidieron la factura [${this.invoiceId}]</h2>
			<div>y el total es: ${this.invoice && this.invoice.total ? this.invoice.total.ved : 'tiriri'} Bs</div>
		`;
	}
	/**
	 * Es llamado por vaading antes de entrar a la ruta
	 */
	async onBeforeEnter(location) {
		// TODO: Validar que sea un número antes de llamar a Number
		this.invoiceId = Number(location.params.invoiceId);
		/** Cargando el poteService desde el global */
		const poteService = global.poteService;
		/** Llamando al método para recuperar la factura */
		poteService.recoverDraftInvoice$(this.invoiceId)
			.pipe(
				tap(invoice => this.invoice = invoice),
				tap(invoice => this.invoiceId = invoice.id),
				tap( () => this.requestUpdate()),
				take(1),
			)
			.subscribe();
	}


}



customElements.define('ceco-invoice-detail', InvoiceDetailPage)